import React, { Component } from 'react';
import dJSON from 'dirty-json';
import VehicleList from './Components/VehicleList.jsx';
import { Container, Row, Col, Form, InputGroup } from 'react-bootstrap';
import expand from 'expand-range';

export default class App extends Component {

    constructor(props) {
        super(props);
        this.state = {
            vehicles: [],
            filters: [],
            url: 'http://recruitment.warpdevelopment.co.za/vehicles/vehicles.json',
            makeValue: 'Any',
            bodyValue: 'Any',
            priceFromValue: 'Any',
            priceToValue: 'Any'
        }
    }

    async fetchVehicles() {
        try {
            const proxyurl = 'https://cors-anywhere.herokuapp.com/';
            const data = await fetch(proxyurl + this.state.url);
            const jsonText = await data.text();

            // clean up invalid json
            const cleanData = dJSON.parse(jsonText);

            // set state
            this.setState({
                vehicles:cleanData.data
            });
        } catch (error) {
            console.log(error);
        }
    }

    componentDidMount() {
        this.fetchVehicles();
    }

    changeFilter() {
        let mValue = document.getElementById('formFilterMake').value,
            bValue = document.getElementById('formFilterBody').value,
            fValue = document.getElementById('formFilterPriceFrom').value,
            tValue = document.getElementById('formFilterPriceTo').value;

        this.setState({
            filters: [
                {
                'make': mValue,
                'body': bValue,
                'from': fValue,
                'to': tValue
                }
            ]
        });
    }

    render() {

        // remove duplicates
        // from manufacturers and
        // body types and sort
        // alphabetically
        let allManufactures = [];
        let allBodyTypes = [];
        this.state.vehicles.map((vehicle) => {
            return (
                allManufactures.push(vehicle.manufacturer),
                allBodyTypes.push(vehicle.body)
            );
        });
        const uniqueManufactures = Array.from(new Set(allManufactures)).sort();
        const uniqueBodyTypes = Array.from(new Set(allBodyTypes)).sort();

        // create new arrays for
        // price range options
        let priceRangeSmall = expand('100000..900000..50000');
        let priceRangeLarge = expand('1000000..10000000..2000000');

        return (
            <React.Fragment>
                <Container>
                    <Row>
                        <Col md={9}>
                            <h1>Browse Vehicles</h1>
                        </Col>
                    </Row>
                    <Row>
                        <Col md={3}>
                            <Form>
                                <Form.Group controlId="formFilterMake">
                                    <Form.Label>Manufacturer</Form.Label>
                                    <Form.Control as="select"
                                        value={this.state.makeValue}
                                        onChange={(e) => this.setState({makeValue: e.target.value}, this.changeFilter)}>
                                        <option value="any">Any</option>
                                        { uniqueManufactures.map((make, index) => {
                                            return (
                                                <option key={index} value={make}>{make}</option>
                                            )
                                        })}
                                    </Form.Control>
                                </Form.Group>
                                <Form.Group controlId="formFilterBody">
                                    <Form.Label>Body Style</Form.Label>
                                    <Form.Control as="select"
                                        value={this.state.bodyValue}
                                        onChange={(e) => this.setState({bodyValue: e.target.value}, this.changeFilter)}>
                                        <option value="any">Any</option>
                                        { uniqueBodyTypes.map((body, index) => {
                                            return (
                                                <option key={index} value={body}>{body}</option>
                                            )
                                        })}
                                    </Form.Control>
                                </Form.Group>
                                <Form.Group controlId="formFilterPrinceRange">
                                    <Form.Label>Price Range</Form.Label>
                                    <Form.Group controlId="formFilterPriceFrom">
                                        <Form.Label>From</Form.Label>
                                        <InputGroup>
                                            <InputGroup.Prepend>
                                                <InputGroup.Text id="rand-from">R</InputGroup.Text>
                                            </InputGroup.Prepend>
                                            <Form.Control as="select"
                                                aria-describedby="rand-from"
                                                value={this.state.priceFromValue}
                                                onChange={(e) => this.setState({priceFromValue: e.target.value}, this.changeFilter)}>
                                                <option value="any">Any</option>
                                                <option value="0">0</option>
                                                { priceRangeSmall.map((number, index) => {
                                                    return (
                                                        <option key={index} value={number}>{number}</option>
                                                    )
                                                })}
                                                { priceRangeLarge.map((number, index) => {
                                                    return (
                                                        <option key={index} value={number}>{number}</option>
                                                    )
                                                })}
                                            </Form.Control>
                                        </InputGroup>
                                    </Form.Group>
                                    <Form.Group controlId="formFilterPriceTo">
                                        <Form.Label>To</Form.Label>
                                        <InputGroup>
                                            <InputGroup.Prepend>
                                                <InputGroup.Text id="rand-from">R</InputGroup.Text>
                                            </InputGroup.Prepend>
                                            <Form.Control as="select"
                                                value={this.state.priceToValue}
                                                onChange={(e) => this.setState({priceToValue: e.target.value}, this.changeFilter)}>
                                                <option value="any">Any</option>
                                                <option value="0">0</option>
                                                { priceRangeSmall.map((number, index) => {
                                                    return (
                                                        <option key={index} value={number}>{number}</option>
                                                    )
                                                })}
                                                { priceRangeLarge.map((number, index) => {
                                                    return (
                                                        <option key={index} value={number}>{number}</option>
                                                    )
                                                })}
                                            </Form.Control>
                                        </InputGroup>
                                    </Form.Group>
                                </Form.Group>
                            </Form>
                        </Col>
                        <Col md={6}>
                            <VehicleList vehicles={this.state.vehicles} filters={this.state.filters} />
                        </Col>
                        <Col md={3}>
                            <h4>This should be a cart</h4>
                        </Col>
                    </Row>
                </Container>
            </React.Fragment>
        )
    }
}