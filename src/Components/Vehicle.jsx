import React, { Component } from 'react';
import { Row, Col, Card, Button } from 'react-bootstrap';

export default class Vehicle extends Component {

    render () {

        // get the props sent
        // from VehicleList.jsx
        const {
            manufacturer,
            model,
            price,
            body,
            wiki,
            img
        } = this.props.vehicle;

        // image base url
        const BASE_URL = 'http://recruitment.warpdevelopment.co.za/vehicles';

        // to currency format
        let rand = new Intl.NumberFormat('af', { style: 'currency', currency: 'ZAR' }).format(price);
        rand = rand.replace(/[a-z]{3}/i, "R").trim();
        
        return (
            <React.Fragment>
                <Card className="mb-3">
                    <Row>
                        <Col lg={4}>
                            <Card.Img src={BASE_URL + img} />
                        </Col>
                        <Col lg={8}>
                            <Card.Body>
                                <Card.Title>Price: {rand}</Card.Title>
                                <Card.Subtitle className="mb-2 text-muted">Make: {manufacturer}</Card.Subtitle>
                                <Card.Subtitle className="mb-2 text-muted">Model: {model}</Card.Subtitle>
                                <Card.Subtitle className="mb-2 text-muted">Body: {body}</Card.Subtitle>
                            </Card.Body>
                        </Col>
                    </Row>
                    <Card.Footer>
                        <Card.Link href={wiki} target="_blank" rel="noopener noreferrer">Learn more</Card.Link>
                        <Button variant="primary" className="mx-2">Add to Cart</Button>
                    </Card.Footer>
                </Card>
            </React.Fragment>
        )
    }

}