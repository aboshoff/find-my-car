import React, { Component } from 'react';
import Vehicle from './Vehicle.jsx';

export default class VehicleList extends Component {

    render() {

        // get the props sent
        // from App.jsx
        const {
            vehicles,
            filters
        } = this.props;

        // set up filtered array
        let filtered = [];

        if (filters.length !== 0) {

            // get filters
            let fMake = filters.map(filter => { return filter.make }).toString();
            let fBody = filters.map(filter => { return filter.body }).toString();
            let fFrom = filters.map(filter => { return filter.from }).toString();
            let fTo = filters.map(filter => { return filter.to }).toString();

            if (fMake !== 'any') {
                
                if (fBody !== 'any') {
                    
                    if (fFrom !== 'any') {

                        // price range FROM to number
                        let fFromNumber = Number(fFrom);

                        if (fTo !== 'any') {

                            // price range TO to number
                            let fToNumber = Number(fTo);

                            // find vehicles by make, body, price from and price to
                            vehicles.map(vehicle => {
                                if (vehicle.manufacturer === fMake && vehicle.body === fBody && vehicle.price >= fFromNumber && vehicle.price <= fToNumber) {
                                    return (
                                        filtered.push(vehicle)
                                    )
                                } 
                            });

                        } else {
                            // find vehicles by make, body and price from
                            vehicles.map(vehicle => {
                                if (vehicle.manufacturer === fMake && vehicle.body === fBody && vehicle.price >= fFromNumber) {
                                    return (
                                        filtered.push(vehicle)
                                    )
                                } 
                            });
                        }
                    } else {

                        if (fTo !== 'any') {

                            // price range TO to number
                            let fToNumber = Number(fTo);

                            // find vehicles by make, body and price to
                            vehicles.map(vehicle => {
                                if (vehicle.manufacturer === fMake && vehicle.body === fBody && vehicle.price <= fToNumber) {
                                    return (
                                        filtered.push(vehicle)
                                    )
                                } 
                            });

                        } else {
                            // find vehicles by make and body
                            vehicles.map(vehicle => {
                                if (vehicle.manufacturer === fMake && vehicle.body === fBody) {
                                    return (
                                        filtered.push(vehicle)
                                    )
                                } 
                            });
                        }
                    }
                } else {

                    if (fFrom !== 'any') {

                        // price range FROM to number
                        let fFromNumber = Number(fFrom);

                        if (fTo !== 'any') {

                            // price range TO to number
                            let fToNumber = Number(fTo);

                            // find vehicles by make, price from and price to
                            vehicles.map(vehicle => {
                                if (vehicle.manufacturer === fMake && vehicle.price >= fFromNumber && vehicle.price <= fToNumber) {
                                    return (
                                        filtered.push(vehicle)
                                    )
                                } 
                            });

                        } else {
                            // find vehicles by make and price from
                            vehicles.map(vehicle => {
                                if (vehicle.manufacturer === fMake && vehicle.price >= fFromNumber) {
                                    return (
                                        filtered.push(vehicle)
                                    )
                                } 
                            });
                        }
                    
                    } else {

                        if (fTo !== 'any') {

                            // price range TO to number
                            let fToNumber = Number(fTo);

                            // find vehicles by make, price from and price to
                            vehicles.map(vehicle => {
                                if (vehicle.manufacturer === fMake && vehicle.price <= fToNumber) {
                                    return (
                                        filtered.push(vehicle)
                                    )
                                } 
                            });

                        } else {
                            // find vehicle by make
                            vehicles.map(vehicle => {
                                if (vehicle.manufacturer === fMake) {
                                    return (
                                        filtered.push(vehicle)
                                    )
                                } 
                            });
                        }
                    }
                }

            } else {

                if (fBody !== 'any') {

                    if (fFrom !== 'any') {

                        // price range FROM to number
                        let fFromNumber = Number(fFrom);

                        if (fTo !== 'any') {

                            // price range TO to number
                            let fToNumber = Number(fTo);

                            // find vehicles by body, price from and price to
                            vehicles.map(vehicle => {
                                if (vehicle.body === fBody && vehicle.price >= fFromNumber && vehicle.price <= fToNumber) {
                                    return (
                                        filtered.push(vehicle)
                                    )
                                } 
                            });

                        } else {
                            // find vehicles by body and price from
                            vehicles.map(vehicle => {
                                if (vehicle.body === fBody && vehicle.price >= fFromNumber) {
                                    return (
                                        filtered.push(vehicle)
                                    )
                                } 
                            });
                        }

                    } else {
                        // find vehicles by body
                        vehicles.map(vehicle => {
                            if (vehicle.body === fBody) {
                                return ( 
                                    filtered.push(vehicle)
                                )
                            }
                        });
                    }

                } else {

                    if (fFrom !== 'any') {

                        // price range FROM to number
                        let fFromNumber = Number(fFrom);

                        if (fTo !== 'any') {

                            // price range TO to number
                            let fToNumber = Number(fTo);

                            // find vehicles by price from and price to
                            vehicles.map(vehicle => {
                                if (vehicle.price >= fFromNumber && vehicle.price <= fToNumber) {
                                    return (
                                        filtered.push(vehicle)
                                    )
                                } 
                            });

                        } else {
                            // find vehicles by price from
                            vehicles.map(vehicle => {
                                if (vehicle.price >= fFromNumber) {
                                    return (
                                        filtered.push(vehicle)
                                    )
                                } 
                            });
                        }

                    } else {

                        if (fTo !== 'any') {

                            // price range TO to number
                            let fToNumber = Number(fTo);

                            // find vehicles by price to
                            vehicles.map(vehicle => {
                                if (vehicle.price <= fToNumber) {
                                    return (
                                        filtered.push(vehicle)
                                    )
                                } 
                            });

                        } else {
                            // reset vehicles
                            filtered = vehicles;
                        }
                    }
                }
            }
        } else {
            // default vehicles
            filtered = vehicles;
        }

        return (
            <React.Fragment>
                {filtered.map((vehicle, index) => {
                    return (
                        <Vehicle key={index} vehicle={vehicle} />
                    )
                })}
            </React.Fragment>
        )

    }

}